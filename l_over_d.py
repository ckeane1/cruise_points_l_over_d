import pandas as pd
import numpy as np
import scipy
import math
import datetime
from plotly import graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px
import os


#
# bounds for cruise
AIRSPEED_TAKEOFF = 30

CLIMB_RATE_UPPER = 1
CLIMB_RATE_LOWER = -1
## DEFINE CONSTANTS OF MARGINS
GPS_AIRSTRIP_ALTITUDE = 70
GPS_GROUNDROLL_MIN = GPS_AIRSTRIP_ALTITUDE + 20

# constants for L/D calculation

PROP_PITCH = 19
PROP_DIAMETER = 1.984

# Maxiumum Takeoff Weight For Flight Test of 20210720-Test_03.csv

test_set_config_dictionary = {
    "20210701_whole" : {'mass': "4500"},
    "20210707_whole" : {'mass':  "5110"},
    "20210715_whole" : {'mass':  "5500"},
    "20210720_whole" : {'mass':  "4500"}, 
    "20210722_whole" : {'mass':  "4500"},
}
# polynomials to fit

cq_19 = [-0.0292, 0.0146, 0.016]
cq_21 = [-0.0299, 0.0179, 0.018]
ct_19 = [-0.1247, -0.0987, 0.2176]
ct_21 = [-0.1322, -0.076, 0.2319]

heat_torque_error = [3.79 * (10**-4), 0.024 , 2.35]


def import_resample_and_label_time(path, usecols, resample_width):

    df = pd.read_csv(path, usecols=usecols)

    ## Convert timestrings under column 'Timestamp' to datetime64 under 'Datetime' and seconds elapsed under 'Elapsed'
    df['Datetime'] = pd.to_datetime(df['Timestamp'], format='%H:%M:%S:%f')
    df.set_index('Datetime', inplace=True)

    ## Resample data to a 1s period using the (mean) of each period
    period_string = str(resample_width) + 's'
    df = df.resample(period_string).mean()
    df['Elapsed'] = (df.index - pd.Timestamp("1970-01-01")) // pd.Timedelta('1ns')
    df['Elapsed'] = (df['Elapsed'] - df['Elapsed'].iloc[0]) / 10**9

    return ( df )

def l_over_d(path,set) :

    """Calculate L/D for specific cruise points.
    Args:
        path: string path of specific test set.
        set: string name of test set .CSV file.
    Returns:
        pd.DataFrame: dataframe of just L/D points with L/D as a column
    """
    index = ['Timestamp']
    velocities = ['True Airspeed', 'Airspeed', 'Ground Speed', 'Climb Rate', 'Angle of Attack']
    positions = ['GPS Altitude']
    angles = ['ECU Roll Angle', 'ECU Pitch Angle', 'ECU Yaw Angle']
    latlong = ['Latitude', 'Longitude']
    epu = ['Pusher EPU A Commanded Torque' , 'Pusher EPU B Commanded Torque','Pusher EPU A Velocity','Pusher EPU B Velocity','Pusher EPU A Motor Temperature 1','Pusher EPU A Motor Temperature 2','Pusher EPU B Motor Temperature 1','Pusher EPU B Motor Temperature 2']

    ## Define sets of the variables depending on where they are going

    usecols = index + velocities + positions + angles + latlong + epu

    differentiables = velocities + positions + angles

    ## Create dataframe with imported test .CSV

    RESAMPLE_WIDTH = 1  ## INTEGER SECONDS

    cruise_points = import_resample_and_label_time(path, usecols, RESAMPLE_WIDTH)

    ## Differentiate the given list of "Differentiables"
    for column in differentiables:
        cruise_points[column + "_differential"] = (cruise_points[column].diff(periods=1) / cruise_points['Elapsed'].diff(periods=1))

    ## Specify limits on the dataframe filters
    airspeed_condition = cruise_points['True Airspeed'] > AIRSPEED_TAKEOFF
    climb_rate_upper = cruise_points['Climb Rate'] < CLIMB_RATE_UPPER
    climb_rate_lower = cruise_points['Climb Rate'] > CLIMB_RATE_LOWER 
    gps_lower_groundroll = cruise_points['GPS Altitude'] > GPS_GROUNDROLL_MIN

    ## Apply the filters
    cruise_points= cruise_points[(airspeed_condition) & (gps_lower_groundroll) & climb_rate_upper & climb_rate_lower]


    cruise_points['row_num'] = [i for i in range(len(cruise_points))]
    cruise_points_average = pd.DataFrame()
    cruise_points["Time Difference"] = cruise_points["Elapsed"].diff()

    ## TRANSLATE INTO FILTER ARGUMENTS

    air_density_list = []
    rps_list = []
    advance_ratio_list = []
    full_advance_ratio_list = []
    true_airspeed_list = []
    climb_rate_list = []
    commanded_torque_list = []
    stator_temp_list = []   
    l_over_d_indexes = []
    l_over_d_values = []
    average_weight_list = []
    done = False

    WEIGHT = int(test_set_config_dictionary[set]['mass'])*0.45359237

    for index, row in cruise_points.iterrows():

        full_advance_ratio_list.append(row['True Airspeed'] / ( 1.94384 * -((row['Pusher EPU A Velocity'] + row['Pusher EPU B Velocity']) / (2 * 60)) * PROP_DIAMETER))

        if len(air_density_list) <= 10:
            
            air_density_list.append(1.225 / ((row['True Airspeed'] / (row['Airspeed']))**2))
            rps_list.append(-(row['Pusher EPU A Velocity'] + row['Pusher EPU B Velocity']) / (2 * 60))
            advance_ratio_list.append(row['True Airspeed']*0.51444444 / ( -((row['Pusher EPU A Velocity'] + row['Pusher EPU B Velocity']) / (2 * 60)) * PROP_DIAMETER))

            true_airspeed_list.append(row['True Airspeed'])
            climb_rate_list.append(row['Climb Rate'])
            commanded_torque_list.append(- ( row['Pusher EPU A Commanded Torque'] + row['Pusher EPU B Commanded Torque'] )) 
            stator_temp_list.append((row['Pusher EPU A Motor Temperature 1'] + row['Pusher EPU A Motor Temperature 2'] + row['Pusher EPU B Motor Temperature 1'] + row['Pusher EPU B Motor Temperature 2']) / 4)
 
        if len(air_density_list) > 10:

            if (abs(advance_ratio_list[-1] - advance_ratio_list[0]) < 0.15):

                air_density_avg = np.average(air_density_list)
                rps_avg = np.average(rps_list)
                advance_ratio_avg = np.average(advance_ratio_list)
                commanded_torque_avg = np.average(commanded_torque_list)
                stator_temp_avg = np.average(stator_temp_list)
                true_airspeed_avg = np.average(true_airspeed_list)
                climb_rate_list_avg = np.average(climb_rate_list)
                average_weight_list.append(WEIGHT)
                pitch_correction = 0
                torque_error = 1
                corrected_torque = commanded_torque_avg - (np.polyval(heat_torque_error, stator_temp_avg) * commanded_torque_avg) / 100
            
            ## Pitch correction to make Qreal match QrotorDB

                while abs(torque_error) > 0.01 and done == False:

                    val_cq_19 = np.polyval(cq_19, advance_ratio_avg)
                    val_cq_21 = np.polyval(cq_21, advance_ratio_avg)

                    cq = np.interp(PROP_PITCH + pitch_correction, [19, 21], [val_cq_19, val_cq_21])

                    torque = (cq * air_density_avg) * (rps_avg**2) * ( (PROP_DIAMETER)**5)

                    torque_error = ((corrected_torque - torque) / torque)

                    pitch_correction += 0.1

                    # done condition
                    if pitch_correction >= 2:
                        done = True

            air_density_list = []
            rps_list = []
            advance_ratio_list = []
            true_airspeed_list = []
            climb_rate_list = []
            commanded_torque_list = []
            stator_temp_list = []    
                    
            if done == True:

                val_ct_19 = np.polyval(ct_19, advance_ratio_avg)
                val_ct_21 = np.polyval(ct_21, advance_ratio_avg)

                ct = np.interp(PROP_PITCH + pitch_correction, [19, 21], [val_ct_19, val_ct_21])

                thrust = ct * air_density_avg * ((rps_avg)**2) * ( (PROP_DIAMETER)**4)

                l_d = (-1) * (WEIGHT* 9.81) / (((climb_rate_list_avg * 1.94384 * WEIGHT * 9.81) / (true_airspeed_avg)) - thrust)

                if 30 > l_d > 0:
                    l_over_d_indexes.append(row['row_num'])
                    l_over_d_values.append(l_d)
                    print(l_d,thrust,true_airspeed_avg,climb_rate_list_avg)
                
    cruise_points["Advance Ratio"] = full_advance_ratio_list


    print(f'Found {len(l_over_d_values)} L/D values.')

    l_over_d_df = cruise_points[cruise_points['row_num'].isin(l_over_d_indexes)]

    l_over_d_df['l_over_d'] = l_over_d_values
    l_over_d_df['Test ID'] = (str(round(WEIGHT)) + " - " + set)
    l_over_d_df['Mass'] = str(round(WEIGHT))
    l_over_d_df["l_over_d_average"] = [np.average(l_over_d_values)]* len(l_over_d_values)
    # l_over_d_df.drop(columns=['row_num'], inplace=True)

    return (l_over_d_df, cruise_points)


############
## MAIN
###########

## ADD A SERIES DATA PARSING FUNCTIONS (above) AND FEED ALIA 250 CSV FORMAT FLIGHT TEST DATA INTO IT

csv_folder_path = os.path.dirname(os.path.realpath(__file__)) + "/usable_csvs"

# List of files in the csv directory

csv_file_name_list = os.listdir(csv_folder_path)

print( "Found " + str(len(csv_file_name_list)) + " tests")


nameout = "l_over_d.csv"

set_list = []
l_over_d_df_list = []

fig1 = make_subplots(rows=1, cols=1, x_title = "Time Elapsed (seconds)" , y_title = "L / D")
fig1.update_layout(title_text = "L / D vs Time Elapsed - Climbrate + / - " + str(round(CLIMB_RATE_UPPER*196.85 )) + " (fpm)")                                                                                                                                         

altitude = np.arange(500 , 7000, 250 )
torque = np.arange(0.3 , 1 , 0.1 )

for set in csv_file_name_list:

    print("Reading in", set, "..." )


    # obstacle_clearance_points_output = obstacle_clearance_points(pattern_points_output[0])
    l_over_d_output = l_over_d(csv_folder_path + "/" +set,set)  
    l_over_d_df = l_over_d_output[0]
    l_over_d_df_list.append(l_over_d_output[0])
    cruise_points = l_over_d_output[1]
    l_over_d_average = l_over_d_output[0]

    fig2 = make_subplots(rows=6, cols=1)

    fig2.add_trace(go.Scatter(x=l_over_d_df['Elapsed'], y=l_over_d_df['Climb Rate']), row=1, col=1)
    fig2.add_trace(go.Scatter(x=cruise_points['Elapsed'], y=cruise_points['GPS Altitude']), row=2, col=1)
    fig2.add_trace(go.Scatter(x=cruise_points['Elapsed'], y=cruise_points['True Airspeed']), row=3, col=1)
    fig2.add_trace(go.Scatter(x=cruise_points['Elapsed'], y=-(cruise_points['Pusher EPU A Velocity'] + cruise_points['Pusher EPU B Velocity']) / (2 * 60)), row=4, col=1)
    fig2.add_trace(go.Scatter(x=cruise_points['Elapsed'], y=cruise_points['Advance Ratio']), row=5, col=1)

    print(l_over_d_df['l_over_d'],l_over_d_df['Elapsed'])
    print(np.average(l_over_d_df['l_over_d']))
    fig2.add_trace(go.Scatter(x=l_over_d_df['Elapsed'], y=l_over_d_df['l_over_d']), row=6, col=1)
    fig2.add_trace(go.Scatter(x=l_over_d_df['Elapsed'], y=l_over_d_df['l_over_d_average']), row=6, col=1)

    fig1.add_trace(go.Scatter( x=l_over_d_df['Elapsed'] , y =l_over_d_df["l_over_d"] , mode='markers'))#, title = "L / D vs Time - Climbrate Limited to (+/-) " +  str(round(CLIMB_RATE_UPPER))  + " (fpm)"))
    fig1.add_trace(go.Scatter( x=l_over_d_df['Elapsed'], y=l_over_d_df['l_over_d_average'] , ))

    
    fig2.show()

l_over_d_df = pd.concat(l_over_d_df_list)


fig3 = px.scatter(l_over_d_df, x="Test ID" , y ="l_over_d_average", color = "Mass" , title = "L / D vs Date-Mass  - Climbrate Limited to (+/-) " +  str(round(CLIMB_RATE_UPPER*196.85 ))  + " (fpm)")
fig1.show()
fig3.show()
#     l_over_d_output_data_list.append(climb_points_output[1])
#     l_over_d_data = climb_points_output[1]
    
#     #z = np.polyfit(climb_data["Average Density Altitude"], climb_data["Average Climb Rate of Point"], 1) # Polynomial fit
#     #p = np.poly1d(z)

#     # z2 = np.polyfit(climb_data["Average Torque of Point"], climb_data["Average Climb Rate of Point"], 1) # Polynomial fit
#     # p2 = np.poly1d(z2)

#     fig7.add_trace(go.Scatter(x=l_over_d_data["Average Torque of Point"], y=l_over_d_data["Average Climb Rate of Point"], mode='markers', name = climb_data["Weight"][0] ), row=1, col=1)
#     # fig7.add_trace(go.Line(x = torque, y=p2(torque),mode = 'lines', name = climb_data["Weight"][0]), row=1, col=1)

#     fig8.add_trace(go.Scatter(x=l_over_d_data["Average Density Altitude"], y=l_over_d_data["Average Climb Rate of Point"],mode = 'markers' , name = climb_data["Weight"][0] ), row=1, col=1)
#     #fig8.add_trace(go.Line(x = altitude, y=p(altitude),mode = 'lines', name = climb_data["Weight"][0] ), row=1, col=1)
# # mode='lines+markers'

    # fig = make_subplots(rows=10, cols=1)
    # fig.add_trace(go.Scatter(x=cruise_points['Elapsed'], y=cruise_points['IMU 3 x Acceleration']), row=1, col=1)
    # fig.add_trace(go.Scatter(x=cruise_points['Elapsed'], y=cruise_points['IMU 3 z Acceleration']), row=2, col=1)
    # fig.add_trace(go.Scatter(x=cruise_points['Elapsed'], y=cruise_points['IMU 3 y Acceleration']), row=3, col=1)
    # fig.add_trace(go.Scatter(x=cruise_points['Elapsed'], y=cruise_points['ECU Yaw Rate']), row=4, col=1)
    # fig.add_trace(go.Scatter(x=cruise_points['Elapsed'], y=cruise_points['ECU Pitch Rate']), row=5, col=1)
    # fig.add_trace(go.Scatter(x=cruise_points['Elapsed'], y=cruise_points['ECU Roll Rate']), row=6, col=1)
    # fig.add_trace(go.Scatter(x=cruise_points['Elapsed'], y=cruise_points['Climb Rate']), row=7, col=1)
    # fig.add_trace(go.Scatter(x=cruise_points['Elapsed'], y=cruise_points['GPS Altitude_differential']), row=8, col=1)
    # fig.add_trace(go.Scatter(x=cruise_points['Elapsed'], y=cruise_points['ECU Roll Angle']), row=9, col=1)


    # l_over_d_df = l_over_d(cruise_points)
    # print(l_over_d_df['l_over_d'],l_over_d_df['Elapsed'])
    # print(np.average(l_over_d_df['l_over_d']))
    # fig.add_trace(go.Scatter(x=cruise_points['Elapsed'], y=l_over_d_df['l_over_d']), row=10, col=1)

    # fig.show()

